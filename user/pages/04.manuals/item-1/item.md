---
title: 'Remove old contact names from Gmail directory'
media_order: background-low-poly.jpg
taxonomy:
    tag:
        - tag1
        - tag2
anchors:
    active: false
tagtitle: h2
hero_overlay: true
hero_showsearch: false
show_searchsidebar: false
show_breadcrumbs: true
content:
    items:
        - '@self.children'
    limit: 12
    order:
        by: date
        dir: desc
    pagination: true
---

Follow these instructions:
1. Login to your Gmail account https://mail.google.com
2. Go to Contacts https://contacts.google.com/
3. Locate a specific contact to be removed.

See this video tutorial for more information
https://drive.google.com/file/d/1pTXvTXipcWEPkFBfTxAP8xbE5j0mM4Bs/view