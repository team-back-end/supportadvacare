<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/plugins/thumb-ratings/thumb-ratings.yaml',
    'modified' => 1574825072,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'callback' => '/thumb-ratings',
        'unique_ip_check' => false,
        'disable_after_vote' => true,
        'readonly' => false,
        'up_bgcolor' => '#00FF00',
        'down_bgcolor' => '#FF0000',
        'up_color' => '#ffffff',
        'down_color' => '#ffffff'
    ]
];
