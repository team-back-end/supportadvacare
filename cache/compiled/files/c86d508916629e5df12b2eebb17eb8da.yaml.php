<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/config/themes/klb4.yaml',
    'modified' => 1574825400,
    'data' => [
        'enabled' => true,
        'production-mode' => true,
        'grid-size' => 'grid-xl',
        'header-fixed' => true,
        'header-animated' => true,
        'header-dark' => false,
        'header-transparent' => false,
        'sticky-footer' => true,
        'blog-page' => '/blog',
        'spectre' => [
            'exp' => false,
            'icons' => false
        ],
        'header-showexternallinks' => true,
        'header-externallinks' => [
            0 => [
                'icon' => 'fa fa-text-height',
                'text' => 'Typography',
                'url' => '/typography',
                'target' => false
            ],
            1 => [
                'icon' => 'fa fa-user-plus',
                'text' => 'Registration',
                'url' => '/registration',
                'target' => false
            ],
            2 => [
                'icon' => 'fa fa-github',
                'text' => 'GitHub',
                'url' => 'https://github.com/lauroguedes/grav-theme-klb4',
                'target' => true
            ],
            3 => [
                'icon' => 'fa fa-bug',
                'text' => 'Issues',
                'url' => 'https://github.com/lauroguedes/grav-theme-klb4/issues',
                'target' => true
            ]
        ]
    ]
];
