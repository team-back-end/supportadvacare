<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://klb4/klb4.yaml',
    'modified' => 1574824192,
    'data' => [
        'enabled' => true,
        'production-mode' => true,
        'grid-size' => 'grid-xl',
        'header-fixed' => true,
        'header-animated' => true,
        'header-dark' => false,
        'header-transparent' => false,
        'sticky-footer' => true,
        'blog-page' => '/blog',
        'spectre' => [
            'exp' => false,
            'icons' => false
        ],
        'header-showexternallinks' => true,
        'header-externallinks' => [
            0 => [
                'text' => 'Typography',
                'url' => '/typography',
                'icon' => 'fa fa-text-height',
                'target' => false
            ],
            1 => [
                'text' => 'Registration',
                'url' => '/registration',
                'icon' => 'fa fa-user-plus',
                'target' => false
            ],
            2 => [
                'text' => 'GitHub',
                'url' => 'https://github.com/lauroguedes/grav-theme-klb4',
                'icon' => 'fa fa-github',
                'target' => true
            ],
            3 => [
                'text' => 'Issues',
                'url' => 'https://github.com/lauroguedes/grav-theme-klb4/issues',
                'icon' => 'fa fa-bug',
                'target' => true
            ]
        ]
    ]
];
