<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/plugins/anchors/anchors.yaml',
    'modified' => 1574824674,
    'data' => [
        'enabled' => true,
        'active' => true,
        'selectors' => 'h1,h2,h3,h4',
        'placement' => 'right',
        'visible' => 'hover',
        'icon' => NULL,
        'class' => NULL,
        'truncate' => 64
    ]
];
