<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/var/www/html/user/config/site.yaml',
    'modified' => 1574824587,
    'data' => [
        'title' => 'Klb4 Theme',
        'author' => [
            'name' => 'Léo WG',
            'email' => 'leowgweb@gmail.com'
        ],
        'taxonomies' => [
            0 => 'tag',
            1 => 'author'
        ],
        'metadata' => [
            'description' => 'Klb4 Theme for knowledgebase, building with Grav. Grav is an easy to use, yet powerful, open source flat-file CMS'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'blog' => [
            'route' => '/blog'
        ]
    ]
];
